﻿using System;
using System.IO;
using TouchSocket.Core;

namespace TouchAdminClient.Common
{
    public class AppConfig
    {
        private const string Path = "AppConfig.json";
        public string IPHost { get; set; } = "127.0.0.1:7789";
        public string VerifyToken { get; set; } = "Dmtp";

        public static AppConfig Create()
        {
            try
            {
                return File.ReadAllText(Path).FromJsonString<AppConfig>();
            }
            catch
            {
                return new AppConfig();
            }

        }

        public void Save()
        {
            File.WriteAllText(Path, this.ToJsonString());
        }
    }
}
