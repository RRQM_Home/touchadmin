﻿using System;
using System.Threading.Tasks;
using TouchAdminProxyForDmtpRpc;
using TouchSocket.Core;
using TouchSocket.Dmtp;
using TouchSocket.Dmtp.Rpc;
using TouchSocket.Rpc;

namespace TouchAdminClient.Remote
{
    public class UserManagerServiceRemote : IUserManagerService
    {
        private readonly MainClient m_client;
        private readonly ILog m_logger;
        private string m_loginToken;

        public UserManagerServiceRemote(MainClient client,ILog logger)
        {
            this.m_client = client;
            this.m_logger = logger;
        }
        public IRpcClient Client => this.m_client.Client.GetDmtpRpcActor();

        public async Task<LoginResponse> LoginAsync(LoginRequest request, IInvokeOption invokeOption = null)
        {
            try
            {
                if (!await m_client.CheckClient())
                {
                    return new LoginResponse() { ResponseCode = ResponseCode.Fail, Message = "未连接到服务器" };
                }
               
                var response = await this.Client.LoginAsync(request);
                if (response.ResponseCode == ResponseCode.Success)
                {
                    this.m_loginToken = response.LoginToken;
                }
                return response;
            }
            catch (Exception ex)
            {
                m_logger.Exception(ex);
                return new LoginResponse() { ResponseCode = ResponseCode.Unknown, Message = "未知异常" };
            }
        }

        public async Task<RegisterResponse> RegisterAsync(RegisterRequest request, IInvokeOption invokeOption = null)
        {
            try
            {
                if (!await this.m_client.CheckClient())
                {
                    return new RegisterResponse() { ResponseCode = ResponseCode.Fail, Message = "未连接到服务器" };
                }

                return await this.Client.RegisterAsync(request);
            }
            catch (Exception ex)
            {
                this.m_logger.Exception(ex);
                return new RegisterResponse() { ResponseCode = ResponseCode.Unknown, Message = "未知异常" };
            }
        }
    }
}
