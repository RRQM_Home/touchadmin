﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TouchAdminClient.Common;
using TouchAdminProxyForDmtpRpc;
using TouchSocket.Core;
using TouchSocket.Dmtp;
using TouchSocket.Dmtp.Rpc;
using TouchSocket.Sockets;

namespace TouchAdminClient.Remote
{
    public class MainClient
    {
        public MainClient(IRegistrator container)
        {
            this.Client = new HttpDmtpClient();
            this.Client.Setup(new TouchSocketConfig()
                .SetRegistrator(container)
                .SetRemoteIPHost(AppConfig.Create().IPHost)
                .SetDmtpOption(new DmtpOption()
                {
                    VerifyToken = AppConfig.Create().VerifyToken
                })
                .ConfigureContainer(a =>
                {
                    a.RegisterSingleton<IUserManagerService, UserManagerServiceRemote>();
                    a.RegisterSingleton<IHttpDmtpClient>(this.Client);
                    a.RegisterSingleton<MainClient>(this);
                    a.AddFileLogger();
                })
                .ConfigurePlugins(a =>
                {
                    a.UseDmtpRpc();

                    //使用重连
                    a.UseDmtpReconnection<HttpDmtpClient>()
                    .UsePolling(TimeSpan.FromSeconds(5))
                    .SetConnectAction(async (c) =>
                    {
                        try
                        {
                            await c.ConnectAsync();
                            c.Logger.Info("连接成功");
                            return true;
                        }
                        catch (Exception ex)
                        {
                            c.Logger.Exception(ex);
                            return false;
                        }
                    })
                    .SetActionForCheck(async (c, i) =>//重新定义检活策略
                    {
                        if (!c.Online)
                        {
                            c.Logger.Warning("非连接状态");
                            return false;
                        }
                        else if (await c.PingAsync())
                        {
                            c.Logger.Info("Ping 成功");
                            return true;
                        }
                        //返回false时可以判断，如果最近活动时间不超过3秒，则猜测客户端确实在忙，所以跳过本次重连
                        else if (DateTime.Now - c.GetLastActiveTime() < TimeSpan.FromSeconds(3))
                        {
                            c.Logger.Warning("Ping 忙");
                            return null;
                        }
                        //否则，直接重连。
                        else
                        {
                            c.Logger.Error("Ping 失败");
                            return false;
                        }
                    });
                }));
        }

        public HttpDmtpClient Client { get; private set; }

        public async Task<bool> CheckClient()
        {
            if (this.Client.Online)
            {
                return true;
            }

            try
            {
                this.Client.Close();
                await this.Client.ConnectAsync();
                return true;
            }
            catch (Exception ex)
            {
                this.Client.Logger.Exception(ex);
                return false;
            }
        }
    }
}
