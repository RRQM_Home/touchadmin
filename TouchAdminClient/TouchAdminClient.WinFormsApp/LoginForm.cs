using TouchAdminProxyForDmtpRpc;
using TouchSocket.Core;

namespace TouchAdminClient.WinFormsApp
{
    public partial class LoginForm : Form
    {
        public LoginForm(IUserManagerService userManagerService,IResolver resolver)
        {
            InitializeComponent();
            this.Load += this.Form1_Load;
            this.m_userManagerService = userManagerService;
            this.m_resolver = resolver;
        }

        private void Form1_Load(object? sender, EventArgs e)
        {
            UpdateText();
        }

        bool m_loginRun = true;
        private readonly IUserManagerService m_userManagerService;
        private readonly IResolver m_resolver;

        public void UpdateText()
        {
            if (m_loginRun)
            {
                this.Text = "��¼";
                this.button1.Text = "��¼";
                this.linkLabel1.Text = "û���˺ţ�ע��";
            }
            else
            {
                this.Text = "ע��";
                this.button1.Text = "ע��";
                this.linkLabel1.Text = "�����˺ţ���¼";
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.m_loginRun = !this.m_loginRun;
            this.UpdateText();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (this.m_loginRun)
            {
                var response = await this.m_userManagerService.LoginAsync(new LoginRequest()
                {
                    Account = this.textBox1.Text,
                    Password = this.textBox2.Text,
                });

                if (response.ResponseCode == ResponseCode.Success)
                {
                    this.m_resolver.Resolve<MainForm>()
                        .Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show(response.Message);
                }
            }
            else
            {
                var response = await this.m_userManagerService.RegisterAsync(new RegisterRequest()
                {
                    Account = this.textBox1.Text,
                    Password = this.textBox2.Text,
                });

                if (response.ResponseCode == ResponseCode.Success)
                {
                    this.m_loginRun = true;
                    this.UpdateText();
                    MessageBox.Show("ע��ɹ�");
                }
                else
                {
                    MessageBox.Show(response.Message);
                }
            }
        }
    }
}