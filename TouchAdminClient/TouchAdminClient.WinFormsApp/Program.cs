using TouchAdminClient.Remote;
using TouchSocket.Core;

namespace TouchAdminClient.WinFormsApp
{
    internal static class Program
    {
        /// <summary>
        /// ����
        /// </summary>
        public static IContainer Container { get; private set; }=new Container();

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var mainClient = new MainClient(Container);
            RegisterType();
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            Application.Run(Container.Resolve<LoginForm>());
        }

        static void RegisterType()
        {
            Container.RegisterSingleton<LoginForm>();
            Container.RegisterSingleton<MainForm>();
        }
    }
}