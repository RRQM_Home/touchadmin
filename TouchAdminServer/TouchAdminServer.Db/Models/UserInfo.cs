﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TouchAdminServer.Db
{
    public class UserInfo
    {
        public string Account { get; set; }

        public string Password { get; set; }
        public string LoginedToken { get; set; }
    }
}
