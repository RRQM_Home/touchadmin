﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace TouchAdminServer.Db
{
    public class UserManagerFromDBForTest: IUserManagerFromDB
    {
        public readonly ConcurrentDictionary<string, UserInfo> RegisterDic = new ConcurrentDictionary<string, UserInfo>();
        public readonly ConcurrentDictionary<string, UserInfo> LoginedDic = new ConcurrentDictionary<string, UserInfo>();

        public bool TryGetRegisteredInfo(string account, out UserInfo userInfo)
        {
            return this.RegisterDic.TryGetValue(account, out userInfo);
        }

        public void Login(string loginedToken, UserInfo userInfo)
        {
            if (!this.LoginedDic.TryAdd(loginedToken, userInfo))
            {
                throw new Exception("账号已登录");
            }
        }

        public void Register(UserInfo userInfo)
        {
            if (!this.RegisterDic.TryAdd(userInfo.Account, userInfo))
            {
                throw new Exception("账号已被注册");
            }
        }

        public bool TryGetLoginedInfo(string loginedToken, out UserInfo userInfo)
        {
            return this.LoginedDic.TryGetValue(loginedToken, out userInfo);
        }
    }
}
