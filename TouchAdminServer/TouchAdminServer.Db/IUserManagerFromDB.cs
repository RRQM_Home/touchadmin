﻿using System;

namespace TouchAdminServer.Db
{
    public interface IUserManagerFromDB
    {
        /// <summary>
        /// 获取账号的注册信息
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        bool TryGetRegisteredInfo(string account, out UserInfo userInfo);

        /// <summary>
        /// 注册账号
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        void Register(UserInfo userInfo);

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userInfo"></param>
        void Login(string loginedToken, UserInfo userInfo);

        /// <summary>
        /// 获取登录账号的信息
        /// </summary>
        /// <param name="userInfo"></param>
        bool TryGetLoginedInfo(string loginedToken, out UserInfo userInfo);
    }
}
