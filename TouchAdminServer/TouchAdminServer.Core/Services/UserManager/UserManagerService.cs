//------------------------------------------------------------------------------
//  此代码版权（除特别声明或在XREF结尾的命名空间的代码）归作者本人若汝棋茗所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/qq_40374647
//  哔哩哔哩视频：https://space.bilibili.com/94253567
//  Gitee源代码仓库：https://gitee.com/RRQM_Home
//  Github源代码仓库：https://github.com/RRQM
//  API首页：http://rrqm_home.gitee.io/touchsocket/
//  交流QQ群：234762506
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using TouchAdminServer.Db;
using TouchSocket.Core;
using TouchSocket.Rpc;

namespace TouchAdminServer.Core
{
    public partial class UserManagerService : RpcServer, IUserManagerService
    {
        private readonly ILog m_logger;
        private readonly IUserManagerFromDB m_userManager;

        public UserManagerService(ILog logger, IUserManagerFromDB userManager)
        {
            this.m_logger = logger;
            this.m_userManager = userManager;
        }

        public LoginResponse Login(LoginRequest request)
        {
            var response = new LoginResponse();
            try
            {
                if (request.Account.IsNullOrEmpty() || request.Password.IsNullOrEmpty())
                {
                    response.ResponseCode = ResponseCode.Fail;
                    response.Message = "账号或密码均不能为空";
                    return response;
                }
                if (!this.m_userManager.TryGetRegisteredInfo(request.Account, out var userInfo))
                {
                    response.ResponseCode = ResponseCode.Fail;
                    response.Message = "没有该账号的注册信息";
                    return response;
                }
                if (!request.Password.Equals(userInfo.Password))
                {
                    response.ResponseCode = ResponseCode.Fail;
                    response.Message = "账号或密码不正确";
                    return response;
                }

                var loginedToken = Guid.NewGuid().ToString();
                this.m_userManager.Login(loginedToken, userInfo);
                userInfo.LoginedToken = loginedToken;
                response.LoginToken = loginedToken;

                response.ResponseCode = ResponseCode.Success;
                response.Message = "登录成功";

                this.m_logger.Info($"账号：{request.Account}成功登录");
            }
            catch (Exception ex)
            {
                response.ResponseCode = ResponseCode.Unknown;
                response.Message = ex.Message;
                this.m_logger.Exception(ex);
            }
            return response;
        }

        public RegisterResponse Register(RegisterRequest request)
        {
            var response = new RegisterResponse();
            try
            {
                if (request.Account.IsNullOrEmpty() || request.Password.IsNullOrEmpty())
                {
                    response.ResponseCode = ResponseCode.Fail;
                    response.Message = "账号或密码均不能为空";
                    return response;
                }
                if (this.m_userManager.TryGetRegisteredInfo(request.Account, out _))
                {
                    return response.FromFail("该账号已被注册");
                }
                this.m_userManager.Register(new UserInfo()
                {
                    Account = request.Account,
                    Password = request.Password
                });

                response.ResponseCode = ResponseCode.Success;
                response.Message = "注册成功";
                this.m_logger.Info($"账号：{request.Account}成功注册");
            }
            catch (Exception ex)
            {
                response.ResponseCode = ResponseCode.Unknown;
                response.Message = ex.Message;
                this.m_logger.Exception(ex);
            }
            return response;
        }
    }
}