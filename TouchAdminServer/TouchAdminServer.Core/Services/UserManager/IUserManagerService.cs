﻿using System.ComponentModel;
using TouchSocket.Rpc;
using TouchSocket.WebApi;

namespace TouchAdminServer.Core
{
    [LoggerFilter]
    public interface IUserManagerService : IRpcServer
    {

        [Description("登录")]
        [WebApi(HttpMethodType.POST, MethodInvoke = true)]
        [AdminDmtpRpc(MethodInvoke = true)]
        LoginResponse Login(LoginRequest request);


        [Description("注册账号")]
        [AdminDmtpRpc(MethodInvoke = true)]
        [WebApi(HttpMethodType.POST, MethodInvoke = true)]
        RegisterResponse Register(RegisterRequest request);
    }
}
