﻿using System;
using TouchAdminServer.Core;

namespace TouchAdminServer.Core
{
    public static class ResponseBaseExtension
    {
        public static TResponse FromSuccess<TResponse>(this TResponse response, string msg = "success") where TResponse : ResponseBase
        {
            response.ResponseCode = ResponseCode.Success;
            response.Message = msg;
            return response;
        }

        public static TResponse FromFail<TResponse>(this TResponse response, string msg = "fail") where TResponse : ResponseBase
        {
            response.ResponseCode = ResponseCode.Fail;
            response.Message = msg;
            return response;
        }

        public static TResponse FromError<TResponse>(this TResponse response, string msg = "error") where TResponse : ResponseBase
        {
            response.ResponseCode = ResponseCode.Error;
            response.Message = msg;
            return response;
        }

        public static TResponse FromError<TResponse>(this TResponse response, Exception ex) where TResponse : ResponseBase
        {
            response.ResponseCode = ResponseCode.Error;
            response.Message = ex.Message;
            return response;
        }
    }
}
