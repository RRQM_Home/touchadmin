﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TouchAdminServer.Core;
using TouchAdminServer.Db;
using TouchSocket.Core;
using TouchSocket.Dmtp;
using TouchSocket.Dmtp.Rpc;
using TouchSocket.Rpc;
using TouchSocket.Sockets;
using TouchSocket.WebApi.Swagger;

namespace TouchAdminServer.App
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var builder = Host.CreateApplicationBuilder(args);

            builder.Services.AddServiceHostedService<IHttpDmtpService, HttpDmtpService>(config =>
            {
                config.SetListenIPHosts(7789)
                   .ConfigureContainer(a =>
                   {
                       a.RegisterSingleton<IUserManagerFromDB, UserManagerFromDBForTest>();

                       a.AddConsoleLogger();

                       a.AddRpcStore(store =>
                       {
                           store.RegisterAllFromTouchAdminServer_Core();
#if DEBUG
                           File.WriteAllText("TouchAdminProxyForDmtpRpc.cs", store.GetProxyCodes("TouchAdminProxyForDmtpRpc", new Type[] { typeof(AdminDmtpRpcAttribute) }));
#endif
                       });
                   })
                   .ConfigurePlugins(a =>
                   {
                       a.UseCheckClear();

                       a.UseDmtpRpc();
                       a.UseWebApi();

                       //a.UseSwagger()//使用Swagger页面
                       //.UseLaunchBrowser();//启动浏览器
                   })
                   .SetDmtpOption(new DmtpOption()
                   {
                       VerifyToken = "Dmtp"
                   });
            });


            var host = builder.Build();
            host.Run();
        }
    }
}